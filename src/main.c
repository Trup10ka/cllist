#include <stdio.h>
#include <stdarg.h>
#include "data/linkedlist.h"
#include "data/node.h"

void free_all(int numberOfFrees, ...) {
    va_list argsListPointer;
    va_start(argsListPointer, numberOfFrees);

    int i;
    for (i = 0; i < numberOfFrees; ++i) {
        free_link_mem(va_arg(argsListPointer, LinkedList*));
        printf("\nMemory freed for linked list %i", i + 1);
    }

    va_end(argsListPointer);
}

int main() {
    Node* node1 = create_alloc_node(54);
    Node* node2 = create_alloc_node(63);
    Node* node3 = create_alloc_node(148);

    struct LinkedList *linked1 = init_linked_h(node1);

    addL_node(node2, linked1);
    addL_node(node3, linked1);
    addL(24, linked1);
    addL(84, linked1);
    rm_frm_linked(1, linked1);

    if (get_from_linked(4, linked1) == INDEX_OUT_OF_BOUNDS) {
        printf("Index you inserted is out of bounds!\n");
    }
    printf("List size is %i; Values in list are: \n", linked1 -> size);
    int pointer;
    for (pointer = 0; pointer < linked1 -> size; pointer++) {
        printf("%i\n", get_from_linked(pointer, linked1) -> value);
    }

    free_all(1, linked1);
    return 0;
}