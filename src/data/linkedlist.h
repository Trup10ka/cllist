#include "types.h"
//
// Created by Trup10ka on 11.10.2023.
//

#ifndef FIRSTPROJECT_LINKEDLIST_H
#define FIRSTPROJECT_LINKEDLIST_H

struct Node* get_from_linked(int index, struct LinkedList* listPointer);

int addL_node(struct Node* node, struct LinkedList* listPointer);

int addL(int value, struct LinkedList* listPointer);

int rm_frm_linked(int index, struct LinkedList* listPointer);

struct LinkedList* init_linked();

struct LinkedList* init_linked_h(struct Node* head);

struct LinkedList* init_linked_val(int value);

void free_link_mem(struct LinkedList* linkedList);


#endif
