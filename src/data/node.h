//
// Created by Trup10ka on 11.10.2023.
//

#ifndef FIRSTPROJECT_NODE_H
#define FIRSTPROJECT_NODE_H

struct Node* create_alloc_node_with_next(int value, struct Node* next);

struct Node* create_alloc_node(int value);

#endif
