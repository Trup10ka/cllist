#include <stdlib.h>
#include "types.h"
//
// Created by Trup10ka on 11.10.2023.
//
struct Node* create_alloc_node_with_next(int value, struct Node* next) {
    struct Node* node = malloc(sizeof(struct Node));
    node -> value = value;
    node -> next = next;
    return node;
}

struct Node* create_alloc_node(int value) {
    return create_alloc_node_with_next(value, NULL);
}