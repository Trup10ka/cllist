#include "types.h"
#include "node.h"
#include <stdlib.h>
//
// Created by Trup10ka on 06.10.2023.
//
/**
 * Gets element from specified index
 * @param index - Index of the element you want to find
 * @param listPointer - Address of the LinkedList you are going to search
 * @return - Node on the given index, or null pointer, might also return INDEX_OUT_OF_BOUNDS pointer
 */
struct Node* get_from_linked(int index, struct LinkedList* listPointer) {

    if (index == 0)
        return listPointer -> head;
    if (index > listPointer -> size - 1 || index < 0)
        return INDEX_OUT_OF_BOUNDS;

    int pointer;
    struct Node* current = listPointer -> head;
    for (pointer = 0; pointer < listPointer -> size; pointer++) {
        if (pointer == index)
            return current;
        current = current -> next;
    }
    return NULL;
}
/**
 * Adds Node to the LinkedList
 * @param node - Node to be added
 * @param listPointer - list in which element will be inserted
 * @return 1 if operation was successful, else 0
 */
int addL_node(struct Node* node, struct LinkedList* listPointer) {
    listPointer -> tail -> next = node;
    listPointer -> tail = node;
    listPointer -> size++;
    return 0;
}
int addL(int value, struct LinkedList* listPointer) {
    struct Node* newNode = create_alloc_node(value);
    listPointer -> tail -> next = newNode;
    listPointer -> tail = newNode;
    listPointer -> size++;
    return 0;
}
static int remove_head(struct LinkedList* listPointer) {
    struct Node* futureHead = listPointer -> head -> next;
    listPointer -> head = NULL;
    listPointer -> head = futureHead;
    listPointer -> size--;
    free(listPointer -> head);
    return 0;
}

static int remove_tail(struct LinkedList* listPointer) {
    struct Node* currentTail = listPointer -> tail;
    struct Node* futureTail = get_from_linked(listPointer -> size - 2, listPointer);
    futureTail -> next = NULL;
    listPointer -> tail = futureTail;
    listPointer -> size--;
    free(currentTail);
    return 0;
}

int rm_frm_linked(int index, struct LinkedList* listPointer) {
    if (index > listPointer -> size - 1 || index < 0)
        return 1;
    if (index == 0)
        return remove_head(listPointer);
    if (index == listPointer -> size - 1)
        return remove_tail(listPointer);

    struct Node* toRemove = get_from_linked(index, listPointer);
    struct Node* previousToRemove = get_from_linked(index - 1, listPointer);

    previousToRemove -> next = toRemove -> next;
    listPointer -> size--;
    free(toRemove);
    return 0;
}
/**
 * Initializes the LinkedList without a head Node (empty LinkedList)
 * @return Empty linked list
 */
struct LinkedList* init_linked() {
    struct LinkedList* linkedList = malloc(sizeof(struct LinkedList));
    linkedList -> head = NULL;
    linkedList -> tail = NULL;
    linkedList -> size = 0;
    return linkedList;
}
/**
 * Initializes a LinkedList with root (head) Node
 * @param head - Node, which will be set as head
 * @return LinkedList with head Node initialized
 */
struct LinkedList* init_linked_h(struct Node* head) {
    struct LinkedList* linkedList = malloc(sizeof(struct LinkedList));
    linkedList -> head = head;
    linkedList -> tail = head;
    linkedList -> size = 1;
    return linkedList;
}

struct LinkedList* init_linked_val(int value) {

    struct Node* head = malloc(sizeof(struct Node));
    head -> value = value;
    head -> next = NULL;

    struct LinkedList* linkedList = malloc(sizeof(struct LinkedList));
    linkedList -> head = head;
    linkedList -> tail = head;
    linkedList -> size = 1;
    return linkedList;
}

void free_link_mem(struct LinkedList* linkedList) {
    if (linkedList -> head == NULL)
        return;
    struct Node* nextNode = linkedList -> head;
    struct Node* current;
    while (nextNode != NULL) {
        current = nextNode;
        nextNode = nextNode -> next;
        free(current);
    }
}