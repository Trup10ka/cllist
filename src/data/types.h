#define INDEX_OUT_OF_BOUNDS ((void *)2)
//
// Created by Trup10ka on 06.10.2023.
//

#ifndef FIRSTPROJECT_TYPES_H
#define FIRSTPROJECT_TYPES_H
/// 4 B unsigned long

typedef struct Node {
    int value;
    struct Node* next;
} Node;
/**
 * Structure representing commonly known data structure <strong>LinkedList</strong>
 */

typedef struct LinkedList {
    int size;
    struct Node* head;
    struct Node* tail;
} LinkedList;

#endif
